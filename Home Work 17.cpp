#include <iostream>
#include <math.h>

class SomeClass
{
private:
	int a;
	int b;
	int c;
	int d;
	int e;
public:
	SomeClass() : a(7), b(9), c(3), d(6),e(7)
	{}
	int GetResult() {
		return (a + b) * c - (d * e);
	}
};

class Vector
{

public:
	Vector() : x(31),y(11),z(3)
	{}
	Vector(double _x, double _y,double _z) : x(_x), y(_y), z(_z)
	{}
	void Show() {
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}
	void Module() {
		double M;
		M = sqrt(x+y+z);
		std::cout << '\n' << M << '\n';
	}
private:
	double x = 0;
	double y = 0;
	double z = 0;
};
int main() {
	SomeClass temp;
	std::cout << temp.GetResult();
	Vector v;
	v.Show();
	v.Module();
}